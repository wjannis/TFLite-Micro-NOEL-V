# Copyright 2022 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# Simple Makefile that build libtflm.a for NOEL-V for RTEMS 6
#
# Execute the following command to build libtflm.a in the directory ./gen:
# make -j8

BUILD_TYPE :=

COMMON_FLAGS := \
  -DTF_LITE_STATIC_MEMORY \
  -fno-unwind-tables \
  -ffunction-sections \
  -fdata-sections \
  -fmessage-length=0
  -march=rv64imafdc \
  -mabi=lp64d \
  -mcmodel=medany \
  -O0

CXX := riscv-rtems6-g++
CC := riscv-rtems6-gcc
AR := riscv-rtems6-ar

INCLUDES := \
  -I. \
  -I./third_party/gemmlowp \
  -I./third_party/flatbuffers/include \
  -I./third_party/kissfft \
  -I./third_party/kissfft/tools \
  -I./third_party/ruy

ifneq ($(TENSORFLOW_ROOT),)
  INCLUDES += -I$(TENSORFLOW_ROOT)
endif

ifneq ($(EXTERNAL_DIR),)
  INCLUDES += -I$(EXTERNAL_DIR)
endif

CXXFLAGS := \
  -std=c++11 \
  -fno-rtti \
  -fno-exceptions \
  -fno-threadsafe-statics \
  -march=rv64imafdc \
  -mabi=lp64d \
  -mcmodel=medany \
  -g \
  -O0 \
  $(COMMON_FLAGS)

CCFLAGS := \
  -std=c11 \
  $(COMMON_FLAGS)

ARFLAGS := -r

GENDIR := gen
OBJDIR := $(GENDIR)/obj
BINDIR := $(GENDIR)/bin
LIB := $(GENDIR)/libtflm.a

TFLM_CC_SRCS := $(shell find $(TENSORFLOW_ROOT)tensorflow -name "*.cc" -o -name "*.c")
OBJS := $(addprefix $(OBJDIR)/, $(patsubst %.c,%.o,$(patsubst %.cc,%.o,$(TFLM_CC_SRCS))))

$(OBJDIR)/%.o: %.cc
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c $< -o $@

$(OBJDIR)/%.o: %.c
	@mkdir -p $(dir $@)
	$(CC) $(CCFLAGS) $(INCLUDES) -c $< -o $@

$(LIB): $(OBJS)
	@mkdir -p $(dir $@)
	$(AR) $(ARFLAGS) $(LIB) $(OBJS)

clean:
	rm -rf $(GENDIR)

libtflm: $(LIB)
